package com.bjongebl.sogeti.asynctaskrotationmethode2;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class NonUITaskFragment extends Fragment {

    MyTask myTask;
    private Activity activity;
    public NonUITaskFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (Activity) context;
        if(myTask != null){
            myTask.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if(myTask != null){
            myTask.onDetach();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return null;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
    }

    public void beginTask(String... arguments){
        myTask = new MyTask(activity);
        myTask.execute(arguments);
    }
}
