package com.bjongebl.sogeti.asynctaskrotationmethode2;

import android.app.Activity;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by BJONGEBL on 15-3-2016.
 */
public class MyTask extends AsyncTask<String, Integer, Boolean> {

    private int contentLength = -1;
    private int counter = 0;
    private int calculatedProgress =0;
    private Activity activity;

    public void onAttach(Activity activity){
        this.activity = activity;
    }

    public void onDetach(){
        activity = null;
    }

    public MyTask(Activity activity) {
        onAttach(activity);
    }

    @Override
    protected void onPreExecute() {
        if(activity != null){
            ((MainActivity) activity).showProgressBarBeforeDownloading();
        }
    }

    @Override
    protected Boolean doInBackground(String... params) {
        boolean successful = false;
        URL downloadURL = null;
        HttpURLConnection httpURLConnection = null;
        InputStream inputStream = null;
        FileOutputStream fileOutputStream = null;
        File file = null;
        try{
            downloadURL = new URL(params[0]);
            httpURLConnection = (HttpURLConnection) downloadURL.openConnection();
            contentLength = httpURLConnection.getContentLength();
            inputStream = httpURLConnection.getInputStream();

            file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath() + "/" + Uri.parse(params[0]).getLastPathSegment());
            fileOutputStream = new FileOutputStream(file);
            Messenger.logMessage(file.getAbsolutePath());
            int read = -1;
            byte[] buffer = new byte[1024];
            while((read = inputStream.read(buffer)) != -1){
                fileOutputStream.write(buffer, 0, read);
                counter = counter + read;
                publishProgress(counter);
            }
            successful = true;
        } catch (IOException e) {
            Messenger.logMessage(e.toString());
        }finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
            } catch (IOException e) {
                Messenger.logMessage(e.toString());
            }
        }
        return successful;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        if(activity == null){
            Messenger.logMessage("Skipping Progress Update Since Activity is null");
        }else{
            calculatedProgress = (int)(((double)values[0]/contentLength)*100);
            ((MainActivity)activity).updateProgress(calculatedProgress);
        }
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        if(activity != null) {
            ((MainActivity) activity).hideProgressBarAfterDownloading();
        }
    }
}
