package com.bjongebl.sogeti.internalstorageexample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ActivityB extends AppCompatActivity {

    TextView userName;
    TextView password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b);
        userName = (TextView) findViewById(R.id.textView2);
        password = (TextView) findViewById(R.id.textView4);
    }

    public void load(View view){
        try {
            FileInputStream fileInputStream = openFileInput("bram.txt");
            int read = -1;
            StringBuffer buffer = new StringBuffer();
            while((read = fileInputStream.read()) != -1){
                buffer.append((char)read);
            }

            Log.d("test", buffer.toString());
            String text1 = buffer.substring(0, buffer.indexOf(" "));
            String text2 = buffer.substring(buffer.indexOf(" ")+1);
            userName.setText(text1);
            password.setText(text2);
            fileInputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Toast.makeText(this, "Load successful", Toast.LENGTH_LONG);
    }

    public void previous(View view){
        Toast.makeText(this,"Previous called", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
