package com.bjongebl.sogeti.backendlessexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.async.callback.BackendlessCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.property.UserProperty;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private final String APPLICATION_ID = "D65EA024-92B7-2607-FF53-5778B9317100";
    private final String SECRET_KEY = "059356B9-2807-82B8-FF78-EC641D7AF600";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String appVersion = "v1";
        Backendless.initApp(this, APPLICATION_ID, SECRET_KEY, appVersion);
        addUser("bram", "bram1234", "Brammos4@live.nl", "01-03-1994");
        login("Brammos4@live.nl", "bram1234");
        describeUserClass();
        logout();
    }

    public void addUser(String name, String password, String email, String birthday) {
        BackendlessUser user = new BackendlessUser();
        user.setProperty("name", name);
        user.setProperty("birthday", birthday);
        user.setEmail(email);
        user.setPassword(password);

        Backendless.UserService.register(user, new BackendlessCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser backendlessUser) {
                Log.i("Registration", backendlessUser.getEmail() + " successfully registered");
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.d("registration", "oww nohh");
            }
        });
    }

    public void login(String username, String password) {
        Backendless.UserService.login(username, password, new
                AsyncCallback<BackendlessUser>() {
                    public void handleResponse(BackendlessUser user) {
                        Log.d("login", "login succesfull");
                    }

                    public void handleFault(BackendlessFault fault) {
                        Log.d("login", "login unsuccesfull");
                    }
                });

    }

    public void logout() {
        Backendless.UserService.logout(new AsyncCallback<Void>() {
            @Override
            public void handleResponse(Void response) {
                Log.d("logout", "logout unsuccesfull");
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.d("logout", "logout unsuccesfull");
            }
        });
    }

    public void describeUserClass() {
        Backendless.UserService.describeUserClass(new AsyncCallback<List<UserProperty>>() {
            public void handleResponse(List<UserProperty> properties) {
                for (UserProperty userProp : properties) {
                    System.out.println("Property name - " + userProp.getName());
                    System.out.println("\trequired - " + userProp.isRequired());
                    System.out.println("\tidentity - " + userProp.isIdentity());
                    System.out.println("\tdata type - " + userProp.getType());
                }
            }

            public void handleFault(BackendlessFault fault) {
            }
        });
    }
}
