package com.bjongebl.sogeti.scrolltab;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentC extends Fragment {


    public FragmentC() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState == null){
            Log.d("test","C onCreate first time");
        }else{
            Log.d("test","C onCreate subsequent time");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_c, container, false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d("test", "C onAttach");
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("test", "C onActivityCreated");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d("test", "C onDetach");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("test", "C onDestroy");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("test", "C onPause");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("test", "C onResume");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("test", "C onStart");

    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("test", "C onStop");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("test", "C onDestroyView");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("test", "C onSaveInstanceState");
    }
}
