package com.example.bram.linearlayoutjavacode;

import android.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    LinearLayout ll;
    TextView t;
    Button b;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ll = new LinearLayout(this);
        t = new TextView(this);
        b = new Button(this);
        ViewGroup.LayoutParams dimensions = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        ll.setLayoutParams(dimensions);

        ViewGroup.LayoutParams viewDimensions = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        t.setLayoutParams(viewDimensions);
        b.setLayoutParams(viewDimensions);

        ll.setOrientation(LinearLayout.VERTICAL);
        t.setText("Hello World");
        b.setText("Button Here");

        ll.addView(t);
        ll.addView(b);
        setContentView(ll);
    }
}
