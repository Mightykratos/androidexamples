package com.bjongebl.sogeti.alertdialogexample;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void showDialog(View view) {
        MyAlert myAlert = new MyAlert();
        myAlert.show(getSupportFragmentManager(),"My Alert");
    }

    public void showFragment(View view) {
        MyAlert myAlert = new MyAlert();
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.group,myAlert,"My Alert Fragment");
        transaction.commit();
    }
}
