package com.bjongebl.sogeti.alertdialogexample;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

/**
 * Created by BJONGEBL on 10-3-2016.
 */
public class MyAlert extends DialogFragment {

//    @NonNull
//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        LayoutInflater inflater = getActivity().getLayoutInflater();
//        View view = inflater.inflate(R.layout.fragment_main, null);
//        builder.setView(view);
//        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                Toast.makeText(getActivity(), "Negative button was clicked", Toast.LENGTH_LONG).show();
//            }
//        });
//        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                Toast.makeText(getActivity(), "Positive button was clicked", Toast.LENGTH_LONG).show();
//            }
//        });
//        //Create custom Alert dialog with code
////        builder.setTitle("My Dialog");
//////        builder.setItems(R.array.days, new DialogInterface.OnClickListener() {
//////            @Override
//////            public void onClick(DialogInterface dialog, int which) {
//////                Toast.makeText(getActivity(), "Item was selected " + which, Toast.LENGTH_SHORT).show();
//////            }
//////        });
////        builder.setMultiChoiceItems(R.array.days, null, new DialogInterface.OnMultiChoiceClickListener() {
////            @Override
////            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
////                Toast.makeText(getActivity(), "Item was selected " + which + " was selected " + isChecked, Toast.LENGTH_SHORT).show();
////            }
////        });
////        builder.setMessage("Do you guys like this dialog?");
////        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
////            @Override
////            public void onClick(DialogInterface dialog, int which) {
////                Toast.makeText(getActivity(), "Negative button was clicked", Toast.LENGTH_LONG).show();
////            }
////        });
////        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
////            @Override
////            public void onClick(DialogInterface dialog, int which) {
////                Toast.makeText(getActivity(), "Positive button was clicked", Toast.LENGTH_LONG).show();
////            }
////        });
//        Dialog dialog = builder.create();
//        return dialog;
//    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main,null);
    }
}
