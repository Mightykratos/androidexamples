package com.bjongebl.sogeti.flexibleuserinterface;

/**
 * Created by BJONGEBL on 9-3-2016.
 */
public interface Communicator {
    public void respond(int i);
}
