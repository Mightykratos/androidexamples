package com.bjongebl.sogeti.flexibleuserinterface;


import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentB extends Fragment {

    TextView text;
    String textString;
    public FragmentB() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if(savedInstanceState == null){
        }else{
            textString = savedInstanceState.getString("text");
        }
        return inflater.inflate(R.layout.fragment_fragment_b, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        text = (TextView) getActivity().findViewById(R.id.textView);
        text.setText(textString);
    }

    public void changeData(int i) {
        Resources res = getResources();
        String[] descriptions = res.getStringArray(R.array.descriptions);
        Log.d("test",descriptions[i]);
        text.setText(descriptions[i]);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("text",text.getText().toString());
    }
}
