package com.bjongebl.sogeti.netwerkcalltorecyclerview.Model;

/**
 * Created by BJONGEBL on 22-3-2016.
 */
public class Medicine {
    private String id;
    private String image;
    private String name;
    private String registrationNumber;
    private String barcode;
    private int value;
    private String ingesting;
    private String pharmaceuticalForm;
    private String informationLeaflet;
    private double cost;

    public Medicine() {
    }

    public Medicine(String id, String image, String name, String registrationNumber, String barcode, int value, String ingesting, String pharmaceuticalForm, String informationLeaflet, double cost) {
        this.id = id;
        this.image = image;
        this.name = name;
        this.registrationNumber = registrationNumber;
        this.barcode = barcode;
        this.value = value;
        this.ingesting = ingesting;
        this.pharmaceuticalForm = pharmaceuticalForm;
        this.informationLeaflet = informationLeaflet;
        this.cost = cost;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getIngesting() {
        return ingesting;
    }

    public void setIngesting(String ingesting) {
        this.ingesting = ingesting;
    }

    public String getPharmaceuticalForm() {
        return pharmaceuticalForm;
    }

    public void setPharmaceuticalForm(String pharmaceuticalForm) {
        this.pharmaceuticalForm = pharmaceuticalForm;
    }

    public String getInformationLeaflet() {
        return informationLeaflet;
    }

    public void setInformationLeaflet(String informationLeaflet) {
        this.informationLeaflet = informationLeaflet;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        String medicine = "ID: " + id + "\n"+
                "Image: " + image + "\n"+
                "Name: " + name + "\n"+
                "Registration Number: " + registrationNumber + "\n"+
                "Barcode: " + barcode + "\n"+
                "Value: " + value + "\n"+
                "Ingesting: " + ingesting + "\n"+
                "PharmaceuticalForm: " + pharmaceuticalForm + "\n"+
                "InformationLeaflet: " + informationLeaflet + "\n"+
                "Cost: " + cost + "\n" + "\n";
        return medicine;
    }
}
