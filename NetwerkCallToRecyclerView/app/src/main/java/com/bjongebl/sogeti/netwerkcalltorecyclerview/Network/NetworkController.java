package com.bjongebl.sogeti.netwerkcalltorecyclerview.Network;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bjongebl.sogeti.netwerkcalltorecyclerview.Model.Medicine;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.bjongebl.sogeti.netwerkcalltorecyclerview.Extras.Defaults.*;
import static com.bjongebl.sogeti.netwerkcalltorecyclerview.Extras.Keys.MedicineEndPoints.*;
import static com.bjongebl.sogeti.netwerkcalltorecyclerview.Extras.UrlEndpoints.*;
import static com.bjongebl.sogeti.netwerkcalltorecyclerview.Extras.Util.*;

/**
 * Created by BJONGEBL on 24-3-2016.
 */
public class NetworkController {

    private ListResponseListener listResponseListener;
    private RequestQueue requestQueue;
    private ArrayList<Medicine> listMedicines;
    private Context context;

    public NetworkController(Context context, RequestQueue requestQueue) {
        this.requestQueue = requestQueue;
        this.context = context;
        listResponseListener = (ListResponseListener) context;
    }

    public void getMedicineRequest(){

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, getRequestUrl(), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response){
                listMedicines = parseMedicineJSONResponse(response);
                if(!listMedicines.isEmpty()) {
                    listResponseListener.listSuccess(listMedicines);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listResponseListener.listUnSuccess(error.toString());
            }
        }){
            //custom header
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("application-id", APPLICATION_ID);
                headers.put("secret-key", SECRET_KEY);
                headers.put("application-type", APPLICATION_TYPE);
                headers.put("content-Type", CONTENT_TYPE);
                return headers;
            }
        };
        int socketTimeout = 30000;// 30 sec
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        requestQueue.add(request);
    }

    public static String getRequestUrl(){
        return URL_BACKENDLESS + URL_VERSION + URL_MEDICINE_TABLE;
    }

    public ArrayList<Medicine> parseMedicineJSONResponse(JSONObject response){
        ArrayList<Medicine> medicines = new ArrayList<>();
        if(response != null && response.length() > 0) {
            try {
                if (response.has(KEY_DATA) && !response.isNull(KEY_DATA)) {
                    JSONArray jsonArray = response.getJSONArray(KEY_DATA);
                    for (int i = 0; i < jsonArray.length(); i++) {

                        String id = DEFAULT_OBJECT_ID;
                        String image = DEFAULT_IMAGE;
                        String name = DEFAULT_NAME;
                        String registrationNumber = DEFAULT_REGISTRATION_NUMBER;
                        String barcode = DEFAULT_BARCODE;
                        int value = DEFAULT_VALUE;
                        String ingesting = DEFAULT_INGESTING;
                        String pharmaceuticalForm = DEFAULT_PHARMACEUTICAL_FORM;
                        String informationLeaflet = DEFAULT_INFORMATION_LEAFLET;
                        double cost = DEFAULT_COST;

                        JSONObject currentItem = (JSONObject) jsonArray.get(i);
                        if(currentItem.has(KEY_OBJECT_ID) && !currentItem.isNull(KEY_OBJECT_ID)) {
                            id = currentItem.getString(KEY_OBJECT_ID);
                        }
                        if(currentItem.has(KEY_IMAGE) && !currentItem.isNull(KEY_IMAGE)) {
                            image = currentItem.getString(KEY_IMAGE);
                        }
                        if(currentItem.has(KEY_NAME) && !currentItem.isNull(KEY_NAME)) {
                            name = currentItem.getString(KEY_NAME);
                        }
                        if(currentItem.has(KEY_REGISTRATION_NUMBER) && !currentItem.isNull(KEY_REGISTRATION_NUMBER)) {
                            registrationNumber = currentItem.getString(KEY_REGISTRATION_NUMBER);
                        }
                        if(currentItem.has(KEY_BARCODE) && !currentItem.isNull(KEY_BARCODE)) {
                            barcode = currentItem.getString(KEY_BARCODE);
                        }
                        if(currentItem.has(KEY_VALUE) && !currentItem.isNull(KEY_VALUE)) {
                            value = currentItem.optInt(KEY_VALUE);
                        }
                        if(currentItem.has(KEY_INGESTING) && !currentItem.isNull(KEY_INGESTING)) {
                            ingesting = currentItem.getString(KEY_INGESTING);
                        }
                        if(currentItem.has(KEY_PHARMACEUTICAL_FORM) && !currentItem.isNull(KEY_PHARMACEUTICAL_FORM)) {
                            pharmaceuticalForm = currentItem.getString(KEY_PHARMACEUTICAL_FORM);
                        }
                        if(currentItem.has(KEY_INFORMATION_LEAFLET) && !currentItem.isNull(KEY_INFORMATION_LEAFLET)) {
                            informationLeaflet = currentItem.getString(KEY_INFORMATION_LEAFLET);
                        }
                        if(currentItem.has(KEY_COST) && !currentItem.isNull(KEY_COST)) {
                            cost = currentItem.optDouble(KEY_COST);
                        }
                        Medicine medicine = new Medicine(id, image, name, registrationNumber, barcode, value, ingesting, pharmaceuticalForm, informationLeaflet, cost);
                        if(!medicine.getName().equals(DEFAULT_NAME) && !medicine.getId().equals(DEFAULT_OBJECT_ID)) {
                            medicines.add(medicine);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return medicines;
    }

    public interface ListResponseListener{
        void listSuccess(ArrayList<Medicine> list);
        void listUnSuccess(String error);
    }
}
