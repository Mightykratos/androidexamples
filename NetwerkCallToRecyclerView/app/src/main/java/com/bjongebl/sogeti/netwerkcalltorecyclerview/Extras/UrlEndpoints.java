package com.bjongebl.sogeti.netwerkcalltorecyclerview.Extras;

/**
 * Created by BJONGEBL on 22-3-2016.
 */
public class UrlEndpoints {

    public static final String URL_BACKENDLESS = "https://api.backendless.com/";
    public static final String URL_VERSION = "v1";
    public static final String URL_MEDICINE_TABLE = "/data/Medicines";
    public static final String URL_CHAR_QUESTION = "?";
    public static final String URL_CHAR_EQUALS = "%3D";
    public static final String URL_CHAR_SINGLE_QUOTE = "%27";
    public static final String URL_CHAR_SPACE = "%20";
    public static final String URL_PARAM_WHERE = "where=";
}
