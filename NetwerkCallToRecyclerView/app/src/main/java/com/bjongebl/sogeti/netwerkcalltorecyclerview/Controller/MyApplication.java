package com.bjongebl.sogeti.netwerkcalltorecyclerview.Controller;

import android.app.Application;
import android.content.Context;

import com.android.volley.RequestQueue;
import com.bjongebl.sogeti.netwerkcalltorecyclerview.Network.VolleySingleton;

/**
 * Created by BJONGEBL on 21-3-2016.
 */
public class MyApplication extends Application {

    private static MyApplication mInstance;
    private static RequestQueue requestQueue;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        requestQueue = VolleySingleton.getInstance().getRequestQueue();
    }

    public static MyApplication getInstance() {
        return mInstance;
    }

    public static Context getAppContext() {
        return mInstance.getApplicationContext();
    }

    public static RequestQueue getRequestQueue(){
        return requestQueue;
    }


}
