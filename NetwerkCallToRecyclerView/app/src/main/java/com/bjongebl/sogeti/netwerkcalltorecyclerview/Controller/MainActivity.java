package com.bjongebl.sogeti.netwerkcalltorecyclerview.Controller;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.bjongebl.sogeti.netwerkcalltorecyclerview.Logging.Monitor;
import com.bjongebl.sogeti.netwerkcalltorecyclerview.Model.Medicine;
import com.bjongebl.sogeti.netwerkcalltorecyclerview.Network.NetworkController;
import com.bjongebl.sogeti.netwerkcalltorecyclerview.R;
import com.bjongebl.sogeti.netwerkcalltorecyclerview.adapters.RecyclerAdapter;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements NetworkController.ListResponseListener {

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        NetworkController networkController = new NetworkController(this, MyApplication.getRequestQueue());
        networkController.getMedicineRequest();
    }

    @Override
    public void listSuccess(ArrayList<Medicine> list) {
        RecyclerAdapter recyclerAdapter = new RecyclerAdapter(this);
        recyclerAdapter.setList(list);
        recyclerView.setAdapter(recyclerAdapter);
    }

    @Override
    public void listUnSuccess(String error) {
        Monitor.Toasting(this,error);
    }
}
