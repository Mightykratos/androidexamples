package com.bjongebl.sogeti.netwerkcalltorecyclerview.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bjongebl.sogeti.netwerkcalltorecyclerview.Model.Medicine;
import com.bjongebl.sogeti.netwerkcalltorecyclerview.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by BJONGEBL on 23-3-2016.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyHolder> {

    private LayoutInflater layoutInflater;
    private ArrayList<Medicine> list;
    private Context context;

    public RecyclerAdapter(Context context) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    public void setList(ArrayList<Medicine> list){
        this.list = list;
        notifyItemRangeChanged(0,list.size());
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.custom_row, parent, false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(MyHolder holder, int position) {
        Medicine currentMedicine = list.get(position);
        holder.title.setText(currentMedicine.getName());
        Picasso.with(context).load(currentMedicine.getImage()).placeholder(R.drawable.deurpje).into(holder.thumbnail);
        holder.id.setText(currentMedicine.getId());
        holder.pharmaceuticalForm.setText(currentMedicine.getPharmaceuticalForm());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class MyHolder extends RecyclerView.ViewHolder{

        private TextView title;
        private TextView id;
        private TextView pharmaceuticalForm;
        private ImageView thumbnail;

        public MyHolder(View itemView) {
            super(itemView);
            thumbnail = (ImageView) itemView.findViewById(R.id.thumbnail);
            title = (TextView) itemView.findViewById(R.id.titleCard);
            id = (TextView) itemView.findViewById(R.id.medicineIdText);
            pharmaceuticalForm = (TextView) itemView.findViewById(R.id.medicinePharmaceuticalFormText);
        }
    }
}
