package com.bjongebl.sogeti.netwerkcalltorecyclerview.Extras;

/**
 * Created by BJONGEBL on 23-3-2016.
 */
public interface Defaults {
    String DEFAULT_IMAGE = "https://api.backendless.com/D65EA024-92B7-2607-FF53-5778B9317100/v1/files/images/deurpje2.png";
    int DEFAULT_VALUE = 0;
    double DEFAULT_COST = 0;
    String DEFAULT_PHARMACEUTICAL_FORM = "NA";
    String DEFAULT_INGESTING = "NA";
    String DEFAULT_INFORMATION_LEAFLET = "NA";
    String DEFAULT_REGISTRATION_NUMBER = "NA";
    String DEFAULT_NAME = "NA";
    String DEFAULT_BARCODE = "NA";
    String DEFAULT_OBJECT_ID = "NA";
}
