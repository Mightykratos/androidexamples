package com.bjongebl.sogeti.sqlitedbupdateanddelete;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private BramDatabaseAdapter bramDatabaseAdapter;
    private EditText userName, password,name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bramDatabaseAdapter = new BramDatabaseAdapter(this);
        userName = (EditText) findViewById(R.id.editText);
        password = (EditText) findViewById(R.id.editText2);
        name = (EditText) findViewById(R.id.editText3);
    }

    public void addUser(View view) {
        String user = userName.getText().toString();
        String pass = password.getText().toString();
        Long id = bramDatabaseAdapter.insertData(user,pass);
        if(id < 0){
            Message.message(this, "Unsuccessful");
        }else{
            Message.message(this, "Successfully Inserted A Row");
        }
    }

    public void viewDetails(View view) {
        String data = bramDatabaseAdapter.getAllData();
        Message.message(this, data);
    }

    public void getDetails(View view) {
        //methode 1
//        String s = name.getText().toString();
//        String result = bramDatabaseAdapter.getData(s);
//        Message.message(this,result);

        //methode 2
        String s = name.getText().toString();
        //username password
        String sub1 = s.substring(0, s.indexOf(" "));
        String sub2 = s.substring(s.indexOf(" ") + 1);
        String result = bramDatabaseAdapter.getData(sub1,sub1);
        Message.message(this,result);
    }

    public void update(View view) {
        bramDatabaseAdapter.updateName("Test","Bram");
    }

    public void delete(View view) {
        int count = bramDatabaseAdapter.deleteRow();
        Message.message(this, String.valueOf(count));
    }
}
