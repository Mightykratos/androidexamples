package com.example.bram.gridview;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MyDialog extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_dialog);
        Intent intent = getIntent();
        if(intent!=null){
            int imageId = intent.getIntExtra("countryImage",R.drawable.canada);
            String countryName = intent.getStringExtra("countryName");
            ImageView myImageView = (ImageView) findViewById(R.id.imageView2);
            myImageView.setImageResource(imageId);
            TextView textView = (TextView) findViewById(R.id.textView);
            textView.setText("This flag belongs to " + countryName);
        }
    }

    public void closeDialog(View view){
        finish();
    }
}
