package com.bjongebl.sogeti.inter_fragmentcommunication;

/**
 * Created by BJONGEBL on 9-3-2016.
 */
public interface Communicator {
    public void respond(String data);
}
