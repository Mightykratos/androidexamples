package com.bjongebl.sogeti.netwerkcallexample.Model;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bjongebl.sogeti.netwerkcallexample.Logging.Monitor;
import com.bjongebl.sogeti.netwerkcallexample.R;
import com.bjongebl.sogeti.netwerkcallexample.Network.VolleySingleton;


public class MainActivity extends AppCompatActivity {

    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://php.net/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Monitor.Toasting(getApplicationContext(),response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Monitor.Toasting(getApplicationContext(), error.toString());
            }
        });
        requestQueue.add(stringRequest);
    }
}
