package com.bjongebl.sogeti.sharedpreferencesexample;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityB extends AppCompatActivity {

    TextView userNameTextView, passwordTextView;
    public static final String DEFAULT = "N/A";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        userNameTextView = (TextView) findViewById(R.id.textView2);
        passwordTextView = (TextView) findViewById(R.id.textView4);
    }

    public void load(View view){
        SharedPreferences sharedPreferences = getSharedPreferences("MyData", Context.MODE_PRIVATE);
        String name = sharedPreferences.getString("name", DEFAULT);
        String password = sharedPreferences.getString("password",DEFAULT);
        if (name.equals(DEFAULT) || password.equals(DEFAULT)){
            Toast.makeText(this,"No Data was Found",Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(this,"Data loaded Succesfully",Toast.LENGTH_LONG).show();
            userNameTextView.setText(name);
            passwordTextView.setText(password);
        }
    }

    public void previous(View view){
        Toast.makeText(this, "Previous",Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

}
