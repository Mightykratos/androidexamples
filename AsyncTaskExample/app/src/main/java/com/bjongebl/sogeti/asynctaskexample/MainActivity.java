package com.bjongebl.sogeti.asynctaskexample;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView mainList;
    private String[] texts = {
            "Lorem", "ipsum","dolor", "sit","amet", "consectetur","adipisicing", "elet","sed", "do",
            "eiusmod", "aliqua","ut", "enim","ad", "minim","veniam", "quis","nostrud", "exercitation",
            "Lorem", "ipsum","dolor", "sit","amet", "consectetur","adipisicing", "elet","sed", "do",
            "eiusmod", "aliqua","ut", "enim","ad", "minim","veniam", "quis","nostrud", "exercitation",
            "Lorem", "ipsum","dolor", "sit","amet", "consectetur","adipisicing", "elet","sed", "do",
            "eiusmod", "aliqua","ut", "enim","ad", "minim","veniam", "quis","nostrud", "exercitation",
            "Lorem", "ipsum","dolor", "sit","amet", "consectetur","adipisicing", "elet","sed", "do",
            "eiusmod", "aliqua","ut", "enim","ad", "minim","veniam", "quis","nostrud", "exercitation",
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_PROGRESS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainList = (ListView) findViewById(R.id.listView1);
        mainList.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,new ArrayList<String>()));
        new MyTask().execute();
    }

    class MyTask extends AsyncTask<Void, String, Void>{

        private ArrayAdapter<String> adapter;
        private int count = 0;
        @Override
        protected void onPreExecute() {
            adapter = (ArrayAdapter<String>) mainList.getAdapter();
            setProgressBarIndeterminate(false);
            setProgressBarIndeterminateVisibility(true);
            setProgressBarVisibility(true);
        }

        @Override
        protected Void doInBackground(Void... params) {
            for (String item : texts) {
                publishProgress(item);
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            adapter.add(values[0]);
            count++;
            setProgress((int)(((double)count/texts.length)*10000));
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            setProgressBarVisibility(false);
            setProgressBarIndeterminateVisibility(false);
            Messenger.toastMessage(MainActivity.this, "All Items were added successfully");
        }
    }
}
