package com.bjongebl.sogeti.netwerkcallheadertoarrayexample.Model;

import android.app.Application;
import android.content.Context;

/**
 * Created by BJONGEBL on 21-3-2016.
 */
public class MyApplication extends Application {

    private static MyApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public static MyApplication getInstance() {
        return mInstance;
    }

    public static Context getAppContext() {
        return mInstance.getApplicationContext();
    }
}
