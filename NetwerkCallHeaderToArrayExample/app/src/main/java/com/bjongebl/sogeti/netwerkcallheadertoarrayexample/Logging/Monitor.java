package com.bjongebl.sogeti.netwerkcallheadertoarrayexample.Logging;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by BJONGEBL on 22-3-2016.
 */
public class Monitor {
    public static void Logging(String message){
        Log.d("log",message);
    }

    public static void Toasting(Context context, String message){
        Toast.makeText(context,message,Toast.LENGTH_LONG).show();
    }
}
