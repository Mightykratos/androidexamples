package com.bjongebl.sogeti.netwerkcallheadertoarrayexample.Network;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.bjongebl.sogeti.netwerkcallheadertoarrayexample.Model.MyApplication;

/**
 * Created by BJONGEBL on 21-3-2016.
 */
public class VolleySingleton {

    private static VolleySingleton mInstance = null;
    private RequestQueue mRequestQueue;

    private VolleySingleton() {
        mRequestQueue = Volley.newRequestQueue(MyApplication.getAppContext());
    }

    public static VolleySingleton getInstance() {
        if(mInstance == null){
            mInstance = new VolleySingleton();
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue(){
        return mRequestQueue;
    }
}
