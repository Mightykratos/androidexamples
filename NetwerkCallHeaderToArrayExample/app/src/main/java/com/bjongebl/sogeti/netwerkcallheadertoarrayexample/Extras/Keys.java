package com.bjongebl.sogeti.netwerkcallheadertoarrayexample.Extras;

/**
 * Created by BJONGEBL on 22-3-2016.
 */
public interface Keys {
    interface MedicineEndPoints {
        String KEY_OFFSET = "offset";
        String KEY_DATA = "data";
        String KEY_IMAGE = "image";
        String KEY_COST = "cost";
        String KEY_PHARMACEUTICAL_FORM = "pharmaceuticalForm";
        String KEY_INGESTING = "ingesting";
        String KEY_CREATED = "created";
        String KEY_INFORMATION_LEAFLET = "informationLeaflet";
        String KEY_OWNERID = "ownerId";
        String KEY_META = "__meta";
        String KEY_REGISTRATION_NUMBER = "registrationNumber";
        String KEY_NAME = "name";
        String KEY_CLASS = "___class";
        String KEY_BARCODE = "barcode";
        String KEY_VALUE = "value";
        String KEY_UPDATED = "updated";
        String KEY_OBJECT_ID = "objectId";
        String KEY_NEXT_PAGE = "nextPage";
        String KEY_TOTAL_OBJECTS = "totalObjects";
    }
}
