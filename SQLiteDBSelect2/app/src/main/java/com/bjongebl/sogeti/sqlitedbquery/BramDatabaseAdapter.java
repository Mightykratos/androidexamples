package com.bjongebl.sogeti.sqlitedbquery;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by BJONGEBL on 14-3-2016.
 */
public class BramDatabaseAdapter {

    BramHelper bramHelper;

    public BramDatabaseAdapter(Context context) {
        bramHelper = new BramHelper(context);
    }

    public long insertData(String name, String password) {
        SQLiteDatabase sqLiteDatabase = bramHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(BramHelper.NAME, name);
        contentValues.put(BramHelper.PASSWORD, password);
        long id = sqLiteDatabase.insert(BramHelper.TABLE_NAME, null, contentValues);
        return id;
    }

    public String getAllData() {
        SQLiteDatabase sqLiteDatabase = bramHelper.getWritableDatabase();
        String[] columns = {BramHelper.UID, BramHelper.NAME, BramHelper.PASSWORD};
        Cursor cursor = sqLiteDatabase.query(BramHelper.TABLE_NAME, columns, null, null, null, null, null, null);
        StringBuffer buffer = new StringBuffer();
        int UIDColumnNumber = cursor.getColumnIndex(BramHelper.UID);
        int NAMEColumnNumber = cursor.getColumnIndex(BramHelper.NAME);
        int PASSWORDColumnNumber = cursor.getColumnIndex(BramHelper.PASSWORD);
        while (cursor.moveToNext()) {
            int cid = cursor.getInt(UIDColumnNumber);
            String userName = cursor.getString(NAMEColumnNumber);
            String password = cursor.getString(PASSWORDColumnNumber);
            buffer.append(cid + " " + userName + " " + password + "\n");
        }
        return buffer.toString();
    }

    public String getData(String name) {
        //Methode 1
//        // select name,password from bramtable where name = 'bram';
        SQLiteDatabase sqLiteDatabase = bramHelper.getWritableDatabase();
        String[] columns = {BramHelper.NAME, BramHelper.PASSWORD};
        Cursor cursor = sqLiteDatabase.query(BramHelper.TABLE_NAME, columns, BramHelper.NAME + " = '" + name + "'", null, null, null, null, null);
        StringBuffer buffer = new StringBuffer();
        int NAMEColumnNumber = cursor.getColumnIndex(BramHelper.NAME);
        int PASSWORDColumnNumber = cursor.getColumnIndex(BramHelper.PASSWORD);
        while (cursor.moveToNext()) {
            String userName = cursor.getString(NAMEColumnNumber);
            String password = cursor.getString(PASSWORDColumnNumber);
            buffer.append(userName + " " + password + "\n");
        }
        return buffer.toString();
    }

    public String getData(String name, String password) {
        //methode2
        // select _id from vivztable where name=? AND password=?;
        SQLiteDatabase sqLiteDatabase = bramHelper.getWritableDatabase();
        String[] columns = {BramHelper.UID};
        String[] sectrionArgs={name,password};
        Cursor cursor = sqLiteDatabase.query(BramHelper.TABLE_NAME, columns, BramHelper.NAME + " =? AND " + BramHelper.PASSWORD + " =?",sectrionArgs, null, null, null, null);
        StringBuffer buffer = new StringBuffer();
        int UIDColumnNumber = cursor.getColumnIndex(BramHelper.UID);
        while (cursor.moveToNext()) {
            int personID = cursor.getInt(UIDColumnNumber);
            buffer.append(personID + "\n");
        }
        return buffer.toString();
    }

    static class BramHelper extends SQLiteOpenHelper {
        private final static String DATABASE_NAME = "bramdatabase";
        private final static String TABLE_NAME = "BRAMTABLE";
        private final static int DATABASE_VERSION = 1;
        private final static String UID = "_id";
        private final static String NAME = "Name";
        private final static String PASSWORD = "Password";
        private final static String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " (" + UID + " INTEGER PRIMARY KEY AUTOINCREMENT," + NAME + " VAR(255), " + PASSWORD + " VARCHAR(255));";
        private final static String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
        private Context context;

        public BramHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            this.context = context;
            Message.message(context, "constructor called");
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            //CREATE TABLE BRAMTABLE (_id INTEGER PRIMARY KEY AUTOINCREMENT, Name VAR(255));
            try {
                db.execSQL(CREATE_TABLE);
                Message.message(context, "onCreate called");
            } catch (SQLException e) {
                Message.message(context, "" + e);
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            try {
                Message.message(context, "onUpgrade called");
                db.execSQL(DROP_TABLE);
                onCreate(db);
            } catch (SQLException e) {
                Message.message(context, "" + e);
            }
        }
    }
}
