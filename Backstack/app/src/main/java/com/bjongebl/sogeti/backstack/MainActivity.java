package com.bjongebl.sogeti.backstack;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements FragmentManager.OnBackStackChangedListener{

    FragmentManager fragmentManager;
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragmentManager = getSupportFragmentManager();
        textView = (TextView) findViewById(R.id.message);
        fragmentManager.addOnBackStackChangedListener(this);
    }

    public void addA(View v){
        FragmentA fragmentA = new FragmentA();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.group,fragmentA,"A");
        transaction.addToBackStack("addA");
        transaction.commit();
    }
    public void addB(View v){
        FragmentB fragmentB = new FragmentB();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.group,fragmentB,"B");
        transaction.addToBackStack("addB");
        transaction.commit();
    }
    public void removeA(View v){
        FragmentA fragmentA = (FragmentA) fragmentManager.findFragmentByTag("A");
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if(fragmentA!=null) {
            transaction.remove(fragmentA);
            transaction.addToBackStack("removeA");
            transaction.commit();
        }else{
            Toast.makeText(this,"The Fragment A was not added befor",Toast.LENGTH_SHORT).show();
        }
    }
    public void removeB(View v){
        FragmentB fragmentB = (FragmentB) fragmentManager.findFragmentByTag("B");
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if(fragmentB!=null) {
            transaction.remove(fragmentB);
            transaction.addToBackStack("removeB");
            transaction.commit();
        }else{
            Toast.makeText(this,"The Fragment B was not added befor",Toast.LENGTH_SHORT).show();
        }
    }

    public void replaceAWithB(View view) {
        FragmentB fragmentB = new FragmentB();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.group,fragmentB,"B");
        transaction.addToBackStack("replaceAWithB");
        transaction.commit();
    }

    public void replaceBwithA(View view) {
        FragmentA fragmentA = new FragmentA();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.group,fragmentA,"A");
        transaction.addToBackStack("replaceBWithA");
        transaction.commit();
    }

    public void attachA(View view) {
        FragmentA fragmentA = (FragmentA) fragmentManager.findFragmentByTag("A");
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if(fragmentA!=null) {
            transaction.attach(fragmentA);
            transaction.addToBackStack("attachA");
            transaction.commit();
        }
    }

    public void detachA(View view) {
        FragmentA fragmentA = (FragmentA) fragmentManager.findFragmentByTag("A");
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if(fragmentA!=null) {
            transaction.detach(fragmentA);
            transaction.addToBackStack("attachB");
            transaction.commit();
        }
    }

    public void back(View view) {
        fragmentManager.popBackStack();
    }

    public void popAddB(View view) {
        fragmentManager.popBackStack("addB",FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Override
    public void onBackStackChanged() {
        textView.setText(textView.getText() + "\n");
        textView.setText(textView.getText() + "The current status of Back Stack" + "\n");
        int count = fragmentManager.getBackStackEntryCount();
        for (int i = count - 1; i >= 0 ; i--) {
            FragmentManager.BackStackEntry backStackEntry = fragmentManager.getBackStackEntryAt(i);
            textView.setText(textView.getText() + " " + backStackEntry.getName() + " \n");
        }
        textView.setText(textView.getText()+"\n");
    }
}
