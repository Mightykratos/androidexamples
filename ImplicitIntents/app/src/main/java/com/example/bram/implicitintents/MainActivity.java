package com.example.bram.implicitintents;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void process(View view){
        Intent intent = null, chooser = null;

        if(view.getId() == R.id.launchMap){
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("geo:19.076,72.8777"));
            chooser = Intent.createChooser(intent, "Launch Maps");
            startActivity(chooser);
        }
        if(view.getId() == R.id.launchMarket){
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("market://details?id=dolphin.developers.com"));
            chooser = Intent.createChooser(intent, "Launch Market");
            startActivity(chooser);
        }
        if(view.getId() == R.id.sentEmail){
            intent = new Intent(Intent.ACTION_SEND);
            intent.setData(Uri.parse("mailto:"));
            String[] to={"Brammos4@live.nl"};
            intent.putExtra(Intent.EXTRA_EMAIL,to);
            intent.putExtra(Intent.EXTRA_SUBJECT, "Hi this is sent from my app");
            intent.putExtra(Intent.EXTRA_TEXT, "Hey, \n Whats up?");
            intent.setType("message/rfc822");
            chooser = Intent.createChooser(intent, "Sent Email");
            startActivity(chooser);
        }
        if (view.getId() == R.id.sentImage) {
            Uri imageUri = Uri.parse("android.recourse://com.example.bram.implicitintents/mipmap/ic_laucher.png/" + R.mipmap.ic_launcher);
            intent = new Intent(Intent.ACTION_SEND);
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_STREAM, imageUri);
            intent.putExtra(Intent.EXTRA_TEXT, "Hey I have attached this image");
            chooser = Intent.createChooser(intent, "Send Image");
            startActivity(chooser);
        }
    }
}
