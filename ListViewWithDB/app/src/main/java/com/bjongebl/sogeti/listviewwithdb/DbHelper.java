package com.bjongebl.sogeti.listviewwithdb;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.provider.BaseColumns;
import android.util.Log;

/**
 * Created by BJONGEBL on 4-3-2016.
 */
public class DbHelper extends SQLiteOpenHelper {

    public static final String TABLE_MEDICINE = "medicine";
    public static final String MEDICINE_NAME = "name";
    public static final String MEDICINE_DESCRIPTION = "description";
    public static final String MEDICINE_IMAGE = "image";
    private static final String DATABASE_NAME = "Medicine.db";
    private static final int DATABASE_VERSION = 1;

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table " + TABLE_MEDICINE + " (_id integer primary key autoincrement," +
                MEDICINE_NAME + " text," +
                MEDICINE_DESCRIPTION + " text," +
                MEDICINE_IMAGE + " text)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table exists" + TABLE_MEDICINE);
        onCreate(db);
    }

    /**
     * Inserts the list of country names into the db.
     * We use SQL transactions for data integrity and efficiency.
     * @param db
     * @return
     */
    public int insertDummyData(SQLiteDatabase db){
        int numInserted = 0;
        db.beginTransaction();
        try {
            String sql = "INSERT INTO " + TABLE_MEDICINE + " (" + MEDICINE_NAME + "," + MEDICINE_DESCRIPTION + "," + MEDICINE_IMAGE +") VALUES (?,?,?)";

            for (int i = 0; i < TITLES.length; i++) {
                SQLiteStatement insert = db.compileStatement(sql);

                insert.bindString(1, TITLES[i]);

                insert.bindString(2, DESCRIPTION[i]);

                insert.bindLong(3, IMAGES[i]);

                insert.execute();

            }
            db.setTransactionSuccessful();
            numInserted = TITLES.length;
        } finally {
            db.endTransaction();
        }
        return numInserted;
    }

    private final String[] TITLES = {"Class aptent taciti sociosqu ad.",
            "Proin a lorem a nisi.",
            "Duis condimentum massa vel erat.",
            "Nullam quis tincidunt purus. Quisque.",
            "Ut lacinia, nisl eget volutpat.",
            "Phasellus interdum ullamcorper nunc, quis.",
            "Ut mollis, tellus vel pulvinar.",
            "Nullam ornare consequat mi, eu.",
            "Interdum et malesuada fames ac.",
            "Nunc eu pellentesque felis. Donec.",
            "Phasellus vitae dolor a lorem.",
            "Sed id pulvinar est, a."};
    private final String[] DESCRIPTION = {"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque dictum nisi nec turpis porttitor egestas. In sit amet tellus non.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras nec faucibus libero. Aliquam turpis mi, tristique at lacus non, rutrum.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sit amet ligula dui. Fusce malesuada, quam sed vehicula convallis, ipsum.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque bibendum, ligula vel eleifend vulputate, elit dolor ornare lorem, sed euismod.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at mauris fermentum, pellentesque justo sed, congue massa. Vivamus eu nisl.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lobortis nunc id urna tincidunt porttitor. Nulla varius eleifend orci, eget.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sit amet justo molestie, cursus odio in, iaculis massa. Praesent aliquet.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque venenatis eget nunc id dictum. Nunc vel arcu nec est tempus.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut luctus sapien pellentesque, luctus ipsum ac, euismod nisi. Proin vel tellus.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque bibendum aliquam vehicula. Donec vitae scelerisque sem, a aliquam ligula. Aenean.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur enim eros, sagittis ac scelerisque nec, porta in dolor. Aenean non.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum vitae lacinia neque. Morbi auctor consectetur tellus in porttitor. Sed venenatis."};
    private final int[] IMAGES = {R.drawable.image1,
            R.drawable.image2,
            R.drawable.image3,
            R.drawable.image1,
            R.drawable.image2,
            R.drawable.image3,
            R.drawable.image1,
            R.drawable.image2,
            R.drawable.image3,
            R.drawable.image1,
            R.drawable.image2,
            R.drawable.image3,
    };
}
