package com.example.bram.checkboxtestproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private CheckBox c;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        c = (CheckBox) findViewById(R.id.checkBox);
        c.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        CheckBox t = (CheckBox) v;
        if(t.isChecked()){
            Toast.makeText(this,"You want coffee with sugar?",Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this,"I know you are diet conscious :D I like it",Toast.LENGTH_SHORT).show();
        }
    }
}
