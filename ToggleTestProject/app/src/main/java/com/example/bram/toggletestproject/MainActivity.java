package com.example.bram.toggletestproject;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {

    RelativeLayout r;
    ToggleButton t;
    TextView tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        t = (ToggleButton) findViewById(R.id.toggleButton);
        t.setOnCheckedChangeListener(this);
        r = (RelativeLayout) findViewById(R.id.main);
        tv = (TextView) findViewById(R.id.textView);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked){
            r.setBackgroundColor(Color.BLACK);
            tv.setTextColor(Color.WHITE);
        }else{
            r.setBackgroundColor(Color.WHITE);
            tv.setTextColor(Color.BLACK);
        }
    }
}
