package com.bjongebl.sogeti.networkcallheadersexample.Model;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bjongebl.sogeti.networkcallheadersexample.Logging.Monitor;
import com.bjongebl.sogeti.networkcallheadersexample.Network.VolleySingleton;
import com.bjongebl.sogeti.networkcallheadersexample.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.bjongebl.sogeti.networkcallheadersexample.Extras.Keys.MedicineEndPoints.*;
import static com.bjongebl.sogeti.networkcallheadersexample.Extras.UrlEndpoints.*;
import static com.bjongebl.sogeti.networkcallheadersexample.Extras.Util.*;


public class MainActivity extends AppCompatActivity {

    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
//        direct netwerk call
//        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://php.net/", new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Toast.makeText(getApplicationContext(), "RESPONSE" + response, Toast.LENGTH_LONG).show();
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(getApplicationContext(),"ERROR" + error.getMessage(), Toast.LENGTH_LONG).show();
//            }
//        });
        //custom netwerk call
        sendJsonRequest();
    }

    public static String getRequestUrl(){
        return URL_BACKENDLESS + URL_VERSION + URL_MEDICINE_TABLE;
    }

    public void sendJsonRequest(){

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, getRequestUrl(), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                parseJSONResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Monitor.Logging(error.toString());
            }
        }){
            //custom header
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("application-id", APPLICATION_ID);
                headers.put("secret-key", SECRET_KEY);
                headers.put("application-type", APPLICATION_TYPE);
                headers.put("content-Type", CONTENT_TYPE);
                return headers;
            }
        };
        int socketTimeout = 30000;// 30 sec
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        requestQueue.add(request);
    }

    public void parseJSONResponse(JSONObject response){
        if(response == null || response.length() == 0){
            return;
        }
        try {
            StringBuilder builder = new StringBuilder();
            if(response.has(KEY_DATA)) {
                JSONArray jsonArray = response.getJSONArray(KEY_DATA);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject currentItem = (JSONObject) jsonArray.get(i);
                    String id = currentItem.getString(KEY_OBJECT_ID);
                    String image = currentItem.getString(KEY_IMAGE);
                    String name = currentItem.getString(KEY_NAME);;
                    String registrationNumber = currentItem.getString(KEY_REGISTRATION_NUMBER);;
                    String barcode = currentItem.getString(KEY_BARCODE);;
                    int value = currentItem.optInt(KEY_VALUE);;
                    String ingesting = currentItem.getString(KEY_INGESTING);;
                    String pharmaceuticalForm = currentItem.getString(KEY_PHARMACEUTICAL_FORM);;
                    String informationLeaflet = currentItem.getString(KEY_INFORMATION_LEAFLET);;
                    double cost = currentItem.optDouble(KEY_COST);
                    builder.append(id + "\t" + image + "\t" + name + "\t" + registrationNumber + "\t" + barcode + "\t" + value + "\t" + ingesting + "\t" + pharmaceuticalForm + "\t" + informationLeaflet + "\t" + cost + "\n");
                }
                Monitor.Logging(builder.toString());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
