package com.sogeti.retrofitrxjava.network;

import com.sogeti.retrofitrxjava.model.Android;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by Bram on 30/01/2017.
 */

public interface RequestInterface {

    @GET("android/jsonarray/")
    Observable<List<Android>> register();
}
