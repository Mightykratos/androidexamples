package com.sogeti.retrofitrxjava.controller.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sogeti.retrofitrxjava.R;
import com.sogeti.retrofitrxjava.model.Android;

import java.util.ArrayList;

/**
 * Created by Bram on 30/01/2017.
 */

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder>{

    private ArrayList<Android> mAndroidList;

    public DataAdapter(ArrayList<Android> androidList){
        mAndroidList = androidList;
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        private TextView mName, mVersion, mApi;

        ViewHolder(View itemView) {
            super(itemView);
            mName = (TextView) itemView.findViewById(R.id.tv_name);
            mVersion = (TextView) itemView.findViewById(R.id.tv_version);
            mApi = (TextView) itemView.findViewById(R.id.tv_api_level);
        }
    }

    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DataAdapter.ViewHolder holder, int position) {
        holder.mName.setText(mAndroidList.get(position).getName());
        holder.mVersion.setText(mAndroidList.get(position).getVer());
        holder.mApi.setText(mAndroidList.get(position).getApi());
    }

    @Override
    public int getItemCount() {
        return mAndroidList != null ? mAndroidList.size() : 0;
    }
}
