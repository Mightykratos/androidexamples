package com.sogeti.retrofitrxjava.model;

import java.util.List;

/**
 * Created by Bram on 31/01/2017.
 */

public class CombinedAndroid {
    private List<Android> android;
    private List<Android> android2;

    public CombinedAndroid(List<Android> android, List<Android> android2) {
        this.android = android;
        this.android2 = android2;
    }

    public List<Android> getAndroid() {
        return android;
    }

    public void setAndroid(List<Android> android) {
        this.android = android;
    }

    public List<Android> getAndroid2() {
        return android2;
    }

    public void setAndroid2(List<Android> android2) {
        this.android2 = android2;
    }
}
