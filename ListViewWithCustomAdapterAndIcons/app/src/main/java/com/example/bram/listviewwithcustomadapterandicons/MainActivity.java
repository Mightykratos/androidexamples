package com.example.bram.listviewwithcustomadapterandicons;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        list = (ListView) findViewById(R.id.listView);
        list.setAdapter(new BramAdapter(this));
    }
}

class SingleRow{
    String title;
    String description;
    int image;

    public SingleRow(String description, String title, int image) {
        this.description = description;
        this.title = title;
        this.image = image;
    }
}

class BramAdapter extends BaseAdapter{

    ArrayList<SingleRow> list;
    Context context;
    BramAdapter(Context context) {
        this.context = context;
        list = new ArrayList<SingleRow>();
        Resources res = context.getResources();
        String[] titles = res.getStringArray(R.array.titles);
        String[] description = res.getStringArray(R.array.descriptions);
        int[] images = {R.drawable.ic_launcher1,R.drawable.ic_launcher2,R.drawable.ic_launcher3,R.drawable.ic_launcher4,
                R.drawable.ic_launcher5,R.drawable.ic_launcher6,R.drawable.ic_launcher7,R.drawable.ic_launcher8,R.drawable.ic_launcher9,R.drawable.ic_launcher10};
        for(int i = 0;i<10;i++){
            list.add(new SingleRow(titles[i],description[i],images[i]));
        }
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.single_row,parent,false);

        TextView title = (TextView) row.findViewById(R.id.textView);
        TextView description = (TextView) row.findViewById(R.id.textView2);
        ImageView image = (ImageView) row.findViewById(R.id.imageView);
        SingleRow temp = list.get(position);
        title.setText(temp.title);
        description.setText(temp.description);
        image.setImageResource(temp.image);

        return row;
    }
}
