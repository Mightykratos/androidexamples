package com.bjongebl.sogeti.swipetab;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity implements ActionBar.TabListener {

    private ActionBar actionbar;
    private ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(new MyAdapter(getSupportFragmentManager()));
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Log.d("test","onPageScrolled at " + " position " + position + " from " + positionOffset + " with number of pixels= " + positionOffsetPixels);
            }

            @Override
            public void onPageSelected(int position) {
                actionbar.setSelectedNavigationItem(position);
                Log.d("test","onPageScrolled at " + " position " + position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    Log.d("test", "onPageScrollStateChanged Idle");
                }
                if (state == ViewPager.SCROLL_STATE_DRAGGING) {
                    Log.d("test", "onPageScrollStateChanged Dragging");
                }
                if (state == ViewPager.SCROLL_STATE_SETTLING) {
                    Log.d("test", "onPageScrollStateChanged Settling");
                }
            }
        });
        actionbar = getActionBar();
        actionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        ActionBar.Tab tab1 = actionbar.newTab();
        tab1.setText("Tab1");
        tab1.setTabListener(this);

        ActionBar.Tab tab2 = actionbar.newTab();
        tab1.setText("Tab2");
        tab1.setTabListener(this);

        ActionBar.Tab tab3 = actionbar.newTab();
        tab1.setText("Tab3");
        tab1.setTabListener(this);

        actionbar.addTab(tab1);
        actionbar.addTab(tab2);
        actionbar.addTab(tab3);
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        viewPager.setCurrentItem(tab.getPosition());
        Log.d("test","onTabReselected at " + " position " + tab.getPosition()+ " name " + tab.getText());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
        Log.d("test","onTabUnselected at " + " position " + tab.getPosition()+ " name " + tab.getText());
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
        Log.d("test","onTabReselected at " + " position " + tab.getPosition()+ " name " + tab.getText());
    }
}

class MyAdapter extends FragmentPagerAdapter{

    public MyAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if(position == 0){
            fragment = new FragmentA();
        }
        if(position == 1){
            fragment = new FragmentB();
        }
        if(position == 2){
            fragment = new FragmentC();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
