package com.bjongebl.sogeti.sqlitedbselect;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private BramDatabaseAdapter bramDatabaseAdapter;
    private EditText userName, password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bramDatabaseAdapter = new BramDatabaseAdapter(this);
        userName = (EditText) findViewById(R.id.editText);
        password = (EditText) findViewById(R.id.editText2);
    }

    public void addUser(View view) {
        String user = userName.getText().toString();
        String pass = password.getText().toString();
        Long id = bramDatabaseAdapter.insertData(user,pass);
        if(id < 0){
            Message.message(this, "Unsuccessful");
        }else{
            Message.message(this, "Successfully Inserted A Row");
        }
    }

    public void viewDetails(View view) {
        String data = bramDatabaseAdapter.getAllData();
        Message.message(this, data);
    }
}
