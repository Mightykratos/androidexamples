package com.bjongebl.sogeti.sqlitedbselect;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by BJONGEBL on 14-3-2016.
 */
public class Message {
    public static void message(Context context, String message) {
        Toast.makeText(context,message,Toast.LENGTH_LONG).show();
    }
}
