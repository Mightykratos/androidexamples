package com.bjongebl.sogeti.cacheandexternalstorage;

import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ActivityB extends AppCompatActivity {

    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b);
        editText = (EditText) findViewById(R.id.editText);
    }

    public void previous(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void loadExternalPublic(View view) {
        File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File myFile = new File(folder,"mydata4.txt");
        String data = readData(myFile);
        if(data != null){
            editText.setText(data);
        }else{
            editText.setText("No data was returned");
        }
    }

    public void loadExternalPrivate(View view) {
        File folder = getExternalFilesDir("bram");
        File myFile = new File(folder,"mydata3.txt");
        String data = readData(myFile);
        if(data != null){
            editText.setText(data);
        }else{
            editText.setText("No data was returned");
        }
    }

    public void loadExternalCache(View view) {
        File folder = getExternalCacheDir();
        File myFile = new File(folder,"mydata2.txt");
        String data = readData(myFile);
        if(data != null){
            editText.setText(data);
        }else{
            editText.setText("No data was returned");
        }
    }

    public void loadInternalCache(View view) {
        File folder = getCacheDir();
        File myFile = new File(folder,"mydata1.txt");
        String data = readData(myFile);
        if(data != null){
            editText.setText(data);
        }else{
            editText.setText("No data was returned");
        }
    }

    private String readData(File myFile) {
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(myFile);
            int read = -1;
            StringBuffer stringBuffer = new StringBuffer();
            while ((read = fileInputStream.read()) != -1) {
                stringBuffer.append((char) read);
            }
            return stringBuffer.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileInputStream != null) {
                    fileInputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
