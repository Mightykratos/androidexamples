package com.bjongebl.sogeti.realmexample;

/**
 * Created by BJONGEBL on 7-3-2016.
 */
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Data extends RealmObject {
    @PrimaryKey
    private String data;

    //The time when this item was added to the database
    private long timestamp;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}