package com.bjongebl.sogeti.recyclerviewexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private BramAdapter bramAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        bramAdapter = new BramAdapter(this,getData());
        recyclerView.setAdapter(bramAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    public static List<Information> getData(){
        List<Information> data = new ArrayList<>();
        int icons = R.mipmap.ic_launcher;
        String[] title = {"Bram","Gijs","Niels","Bart","Jan","Arnd"};
        for (int i = 0; i < 100; i++) {
            Information current = new Information();
            current.imageId = icons;
            current.title = title[i%title.length];
            data.add(current);
        }
        return data;
    }
}
