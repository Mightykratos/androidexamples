package com.bjongebl.sogeti.cardviewexample;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private CardViewAdapter mAdapter;
    private List<Card> cardList = new ArrayList<>();
    private final String[] TITLES = {"Class aptent taciti sociosqu ad.",
            "Proin a lorem a nisi.",
            "Duis condimentum massa vel erat.",
            "Nullam quis tincidunt purus. Quisque.",
            "Ut lacinia, nisl eget volutpat.",
            "Phasellus interdum ullamcorper nunc, quis.",
            "Ut mollis, tellus vel pulvinar.",
            "Nullam ornare consequat mi, eu.",
            "Interdum et malesuada fames ac.",
            "Nunc eu pellentesque felis. Donec.",
            "Phasellus vitae dolor a lorem.",
            "Sed id pulvinar est, a."};
    private final String[] DESCRIPTION = {"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque dictum nisi nec turpis porttitor egestas. In sit amet tellus non.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras nec faucibus libero. Aliquam turpis mi, tristique at lacus non, rutrum.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sit amet ligula dui. Fusce malesuada, quam sed vehicula convallis, ipsum.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque bibendum, ligula vel eleifend vulputate, elit dolor ornare lorem, sed euismod.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at mauris fermentum, pellentesque justo sed, congue massa. Vivamus eu nisl.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lobortis nunc id urna tincidunt porttitor. Nulla varius eleifend orci, eget.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sit amet justo molestie, cursus odio in, iaculis massa. Praesent aliquet.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque venenatis eget nunc id dictum. Nunc vel arcu nec est tempus.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut luctus sapien pellentesque, luctus ipsum ac, euismod nisi. Proin vel tellus.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque bibendum aliquam vehicula. Donec vitae scelerisque sem, a aliquam ligula. Aenean.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur enim eros, sagittis ac scelerisque nec, porta in dolor. Aenean non.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum vitae lacinia neque. Morbi auctor consectetur tellus in porttitor. Sed venenatis."};
    private final int[] IMAGES = {R.drawable.image1,
            R.drawable.image2,
            R.drawable.image3,
            R.drawable.image1,
            R.drawable.image2,
            R.drawable.image3,
            R.drawable.image1,
            R.drawable.image2,
            R.drawable.image3,
            R.drawable.image1,
            R.drawable.image2,
            R.drawable.image3,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        // specify an adapter (see also next example)
        mAdapter = new CardViewAdapter(cardList);
        mRecyclerView.setAdapter(mAdapter);
        addAllCards();
        mAdapter.sortAlphabetical();
    }

    private void addAllCards() {
        for (int i = 0; i < TITLES.length; i++) {
            Card card = new Card(TITLES[i], DESCRIPTION[i], IMAGES[i]);
            cardList.add(card);
        }
        mAdapter.notifyDataSetChanged();
    }

    private void addCard(Card card) {
        cardList.add(card);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}

class CardViewAdapter extends RecyclerView.Adapter<CardViewAdapter.ViewHolder> {

    private List<Card> cardList;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView title;
        public TextView description;
        public ImageView image;

        public ViewHolder(View cardLayout) {
            super(cardLayout);
            title = (TextView) cardLayout.findViewById(R.id.titleCard);
            description = (TextView) cardLayout.findViewById(R.id.descriptionCard);
            image = (ImageView) cardLayout.findViewById(R.id.thumbnail);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public CardViewAdapter(List<Card> cardList) {
        this.cardList = cardList;
    }

    public void sortAlphabetical() {
        Collections.sort(cardList, new Comparator<Card>() {
            @Override
            public int compare(Card lhs, Card rhs) {
                return (lhs.getTitle().compareTo(rhs.getTitle()));
            }
        });
        notifyDataSetChanged();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        // create a new item view
        View cardLayout = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view_item, parent, false);
        return new ViewHolder(cardLayout);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        ViewHolder viewHolder = (ViewHolder) holder;
        Card card = cardList.get(position);
        viewHolder.title.setText(card.getTitle());
        viewHolder.description.setText(card.getDescription());
        viewHolder.image.setImageResource(card.getImage());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return cardList.size();
    }
}
