package com.example.bram.listviewwithicons2;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    String[] memeTitles;
    String[] memeDescriptions;
    int[] images = {R.drawable.ic_launcher, R.drawable.ic_launcher2, R.drawable.ic_launcher3, R.drawable.ic_launcher4, R.drawable.ic_launcher5,
            R.drawable.ic_launcher6, R.drawable.ic_launcher7, R.drawable.ic_launcher8, R.drawable.ic_launcher9, R.drawable.ic_launcher10};
    ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lv = (ListView) findViewById(R.id.listView);
        Resources res = getResources();
        memeTitles = res.getStringArray(R.array.titles);
        memeDescriptions = res.getStringArray(R.array.descriptions);
        CustomAdapter adapter = new CustomAdapter(this, memeTitles, images, memeDescriptions);
        lv.setAdapter(adapter);
    }
}

class CustomAdapter extends ArrayAdapter<String> {
    Context context;
    String[] titleArray;
    String[] descriptionArray;
    int[] images;

    CustomAdapter(Context context, String[] titles, int[] imgs, String[] desc) {
        super(context, R.layout.single_row, R.id.textView, titles);
        this.context = context;
        this.titleArray = titles;
        this.descriptionArray = desc;
        this.images = imgs;
    }

    class myViewHolder {
        ImageView myImage;
        TextView myTitle;
        TextView myDescription;

        myViewHolder(View view) {
            myImage = (ImageView) view.findViewById(R.id.imageView);
            myTitle = (TextView) view.findViewById(R.id.textView);
            myDescription = (TextView) view.findViewById(R.id.textView2);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        myViewHolder holder = null;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.single_row, parent, false);
            holder = new myViewHolder(row);
            row.setTag(holder);
            Log.d("TEST","Creating a new row");
        } else {
            holder = (myViewHolder) row.getTag();
            Log.d("TEST","Recycling a new row");
        }
        holder.myImage.setImageResource(images[position]);
        holder.myTitle.setText(titleArray[position]);
        holder.myDescription.setText(descriptionArray[position]);
        return row;
    }
}