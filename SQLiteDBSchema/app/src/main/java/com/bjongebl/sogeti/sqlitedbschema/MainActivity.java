package com.bjongebl.sogeti.sqlitedbschema;

import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    BramHelper bramHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bramHelper = new BramHelper(this);
        SQLiteDatabase sqLiteDatabase = bramHelper.getWritableDatabase();
    }
}
