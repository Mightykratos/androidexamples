package com.bjongebl.sogeti.sqlitedbschema;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by BJONGEBL on 14-3-2016.
 */
public class BramHelper extends SQLiteOpenHelper {

    private final static String DATABASE_NAME = "bramdatabase";
    private final static String TABLE_NAME = "BRAMTABLE";
    private final static int DATABASE_VERSION = 1;
    private final static String UID = "_id";
    private final static String NAME = "Name";
    private final static String ADDRESS = "Address";
    private final static String CREATE_TABLE ="CREATE TABLE " + TABLE_NAME + " (" + UID + " INTEGER PRIMARY KEY AUTOINCREMENT," + NAME + " VAR(255), " + ADDRESS + " VARCHAR(255));";
    private final static String DROP_TABLE ="DROP TABLE IF EXISTS " + TABLE_NAME;
    private Context context;

    public BramHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        Message.message(context, "constructor called");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //CREATE TABLE BRAMTABLE (_id INTEGER PRIMARY KEY AUTOINCREMENT, Name VAR(255));
        try {
            db.execSQL(CREATE_TABLE);
            Message.message(context, "onCreate called");
        }catch (SQLException e){
            Message.message(context, "" + e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            Message.message(context, "onUpgrade called");
            db.execSQL(DROP_TABLE);
            onCreate(db);
        }catch (SQLException e){
            Message.message(context, "" + e);
        }
    }
}
