package com.example.bram.relativelayoutinjavacode;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.Button;
import android.widget.EditText;
import android.view.ViewGroup.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    RelativeLayout main;
    EditText userNameValue, passwordValue;
    TextView message, userName, password;
    Button login;
    RelativeLayout.LayoutParams messageDimensions, userNameDimensions, userNameValueDimensions, passwordDimensions, passwordValueDimensions, loginDimensions;
    int messageId = 1, userNameId = 2, userNameValueId = 3, passwordId = 4, passwordValueId = 5, loginId = 6;
    int paddingValue = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
        createMessageTextView();
        createUserNameTextView();
        createUserNameEditText();
        createPasswordTextView();
        createPasswordEditText();
        createButton();
        main.addView(message, messageDimensions);
        main.addView(userName, userNameDimensions);
        main.addView(userNameValue, userNameValueDimensions);
        main.addView(password, passwordDimensions);
        main.addView(passwordValue, passwordValueDimensions);
        main.addView(login, loginDimensions);
        setContentView(main);
    }

    private void init() {
        main = new RelativeLayout(this);
        RelativeLayout.LayoutParams mainDimensions = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        main.setLayoutParams(mainDimensions);
        userNameValue = new EditText(this);// here comes the edit text with user name
        passwordValue = new EditText(this);
        message = new TextView(this);
        userName = new TextView(this);
        password = new TextView(this);
        login = new Button(this);
    }

    private void createMessageTextView() {
        messageDimensions = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        messageDimensions.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        message.setText("Please Login First");
        message.setPadding(paddingValue, 100, paddingValue, paddingValue);
        message.setId(messageId);
        message.setLayoutParams(messageDimensions);
    }

    private void createUserNameTextView() {
        userNameDimensions = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        userNameDimensions.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        userNameDimensions.addRule(RelativeLayout.BELOW, messageId);
        userName.setPadding(paddingValue, paddingValue, paddingValue, paddingValue);
        userName.setText("User Name");
        userName.setId(userNameId);
    }

    private void createUserNameEditText() {
        userNameValueDimensions = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        userNameValue.setId(userNameValueId);
        userNameValueDimensions.addRule(RelativeLayout.BELOW, messageId);
        userNameValueDimensions.addRule(RelativeLayout.RIGHT_OF, userNameId);
        userNameValueDimensions.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        userNameValueDimensions.addRule(RelativeLayout.ALIGN_BASELINE, userNameId);
    }

    private void createPasswordTextView() {
        passwordDimensions = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        passwordDimensions.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        passwordDimensions.addRule(RelativeLayout.BELOW, userNameValueId);
        passwordDimensions.addRule(RelativeLayout.ALIGN_RIGHT, userNameId);
        password.setGravity(Gravity.RIGHT);
        password.setPadding(paddingValue, paddingValue, paddingValue, paddingValue);
        password.setText("Password");
        password.setId(passwordId);
    }

    private void createPasswordEditText() {
        passwordValueDimensions = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        passwordValue.setId(passwordValueId);
        passwordValueDimensions.addRule(RelativeLayout.RIGHT_OF, passwordId);
        passwordValueDimensions.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        passwordValueDimensions.addRule(RelativeLayout.BELOW, userNameValueId);
        passwordValueDimensions.addRule(RelativeLayout.ALIGN_BASELINE, passwordId);
    }

    private void createButton() {
        loginDimensions = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        login.setText("Login");
        loginDimensions.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        loginDimensions.addRule(RelativeLayout.BELOW, passwordId);
    }
}
