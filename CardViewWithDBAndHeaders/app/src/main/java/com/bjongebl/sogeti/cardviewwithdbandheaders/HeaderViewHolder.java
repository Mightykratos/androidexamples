package com.bjongebl.sogeti.cardviewwithdbandheaders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by BJONGEBL on 10-3-2016.
 */
public class HeaderViewHolder extends RecyclerView.ViewHolder{
    public TextView mTitle;
    public HeaderViewHolder(View view) {
        super(view);
        mTitle = (TextView)view.findViewById(R.id.header_title);
    }
}
