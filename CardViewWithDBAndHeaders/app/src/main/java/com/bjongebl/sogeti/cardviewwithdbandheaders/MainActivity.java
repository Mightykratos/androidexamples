package com.bjongebl.sogeti.cardviewwithdbandheaders;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AlphabetIndexer;
import android.widget.ImageView;
import android.widget.SectionIndexer;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>{

    private static final int TYPE_HEADER = 1;
    private static final int TYPE_NORMAL = 0;
    private static final int TYPE_COUNT = 2;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private CardViewAdapter mCardViewAdapter;
    private List<Card> cardList = new ArrayList<>();
    private SQLiteDatabase db;
    private AlphabetIndexer indexer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //NOTE: you should never actually start database operations from the context of the UI thread
        final DbHelper helper = new DbHelper(this);
        db = helper.getWritableDatabase();

        //populate the db with our dummy data, might take a while, so in real scenarios spawn a
        //new thread to do this
        final int result = helper.insertDummyData(db);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        // specify an adapter (see also next example)
        final Cursor cursor = db.query(DbHelper.TABLE_MEDICINE, null, null,
                null, null, null, DbHelper.MEDICINE_NAME + " ASC");


        MyListCursorAdapter tmp = new MyListCursorAdapter(this,cursor);
        mRecyclerView.setAdapter(tmp);
//        addAllCards();
    }

//    private void addAllCards() {
//        for (int i = 0; i < TITLES.length; i++) {
//            Card card = new Card(TITLES[i], DESCRIPTION[i], IMAGES[i]);
//            cardList.add(card);
//        }
//        mCardViewAdapter.notifyDataSetChanged();
//    }
//
//    private void addCard(Card card) {
//        cardList.add(card);
//        mCardViewAdapter.notifyDataSetChanged();
//    }

    @Override
    protected void onDestroy() {
        db.close();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        // A switch-case is useful when dealing with multiple Loaders/IDs
        switch (loader.getId()) {
            case 1:
                // The asynchronous load is complete and the data
                // is now available for use. Only now can we associate
                // the queried Cursor with the SimpleCursorAdapter.
                mCardViewAdapter.swapCursor(cursor);
                break;
        }
        // The listview now displays the queried data.
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // For whatever reason, the Loader's data is now unavailable.
        // Remove any references to the old data by replacing it with
        // a null Cursor.
        mCardViewAdapter.swapCursor(null);
    }
}

class CardViewAdapter extends RecyclerView.Adapter<CardViewAdapter.ViewHolder> implements SectionIndexer {

    private List<Card> cardList;
    private int[] usedSectionNumbers;
    private AlphabetIndexer indexer;
    private Map<Integer, Integer> sectionToOffset;
    private Map<Integer, Integer> sectionToPosition;
    private Cursor mCursor;
    protected ChangeObserver mChangeObserver;
    protected DataSetObserver mDataSetObserver;
    protected int mRowIDColumn;
    protected boolean mDataValid;
    protected boolean mAutoRequery;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView title;
        public TextView description;
        public ImageView image;

        public ViewHolder(View cardLayout) {
            super(cardLayout);
            title = (TextView) cardLayout.findViewById(R.id.titleCard);
            description = (TextView) cardLayout.findViewById(R.id.descriptionCard);
            image = (ImageView) cardLayout.findViewById(R.id.thumbnail);
        }
    }

    public CardViewAdapter(Context context, int layout, Cursor c,
                                 String[] from, int[] to) {
        indexer = new AlphabetIndexer(c, c.getColumnIndexOrThrow(DbHelper.MEDICINE_NAME), "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        sectionToPosition = new TreeMap<Integer, Integer>(); //use a TreeMap because we are going to iterate over its keys in sorted order
        sectionToOffset = new HashMap<Integer, Integer>();

        final int count = getItemCount();

        int i;
        //temporarily have a map alphabet section to first index it appears
        //(this map is going to be doing somethine else later)
        for (i = count - 1 ; i >= 0; i--){
            sectionToPosition.put(indexer.getSectionForPosition(i), i);
        }

        i = 0;
        usedSectionNumbers = new int[sectionToPosition.keySet().size()];

        //note that for each section that appears before a position, we must offset our
        //indices by 1, to make room for an alphabetical header in our list
        for (Integer section : sectionToPosition.keySet()){
            sectionToOffset.put(section, i);
            usedSectionNumbers[i] = section;
            i++;
        }

        //use offset to map the alphabet sections to their actual indicies in the list
        for(Integer section: sectionToPosition.keySet()){
            sectionToPosition.put(section, sectionToPosition.get(section) + sectionToOffset.get(section));
        }
    }

    public void sortAlphabetical() {
        Collections.sort(cardList, new Comparator<Card>() {
            @Override
            public int compare(Card lhs, Card rhs) {
                return (lhs.getTitle().compareTo(rhs.getTitle()));
            }
        });
        notifyDataSetChanged();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        // create a new item view
        View cardLayout = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view_item, parent, false);
        return new ViewHolder(cardLayout);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        ViewHolder viewHolder = (ViewHolder) holder;
        Card card = cardList.get(position);
        viewHolder.title.setText(card.getTitle());
        viewHolder.description.setText(card.getDescription());
        viewHolder.image.setImageResource(card.getImage());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {

        if(cardList.size() == 0)
        {
            return cardList.size() + usedSectionNumbers.length;
        }
        return 0;
    }

    @Override
    public Object[] getSections() {
        return indexer.getSections();
    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        if (! sectionToOffset.containsKey(sectionIndex)){
            //This is only the case when the FastScroller is scrolling,
            //and so this section doesn't appear in our data set. The implementation
            //of Fastscroller requires that missing sections have the same index as the
            //beginning of the next non-missing section (or the end of the the list if
            //if the rest of the sections are missing).
            //So, in pictorial example, the sections D and E would appear at position 9
            //and G to Z appear in position 11.
            int i = 0;
            int maxLength = usedSectionNumbers.length;

            //linear scan over the sections (constant number of these) that appear in the
            //data set to find the first used section that is greater than the given section, so in the
            //example D and E correspond to F
            while (i < maxLength && sectionIndex > usedSectionNumbers[i]){
                i++;
            }
            if (i == maxLength) return getItemCount(); //the given section is past all our data

            return indexer.getPositionForSection(usedSectionNumbers[i]) + sectionToOffset.get(usedSectionNumbers[i]);
        }

        return indexer.getPositionForSection(sectionIndex) + sectionToOffset.get(sectionIndex);
    }

    @Override
    public int getSectionForPosition(int position) {
        int i = 0;
        int maxLength = usedSectionNumbers.length;

        //linear scan over the used alphabetical sections' positions
        //to find where the given section fits in
        while (i < maxLength && position >= sectionToPosition.get(usedSectionNumbers[i])){
            i++;
        }
        return usedSectionNumbers[i-1];
    }

    public void changeCursor(Cursor cursor) {
        Cursor old = swapCursor(cursor);
        if (old != null) {
            old.close();
        }
    }

    public Cursor swapCursor(Cursor newCursor) {
        if (newCursor == mCursor) {
            return null;
        }
        Cursor oldCursor = mCursor;
        if (oldCursor != null) {
            if (mChangeObserver != null) oldCursor.unregisterContentObserver(mChangeObserver);
            if (mDataSetObserver != null) oldCursor.unregisterDataSetObserver(mDataSetObserver);
        }
        mCursor = newCursor;
        if (newCursor != null) {
            if (mChangeObserver != null) newCursor.registerContentObserver(mChangeObserver);
            if (mDataSetObserver != null) newCursor.registerDataSetObserver(mDataSetObserver);
            mRowIDColumn = newCursor.getColumnIndexOrThrow("_id");
            mDataValid = true;
            // notify the observers about the new cursor
            notifyDataSetChanged();
        } else {
            mRowIDColumn = -1;
            mDataValid = false;
            // notify the observers about the lack of a data set
            notifyDataSetChanged();
        }
        return oldCursor;
    }

    private class ChangeObserver extends ContentObserver {
        public ChangeObserver() {
            super(new Handler());
        }

        @Override
        public boolean deliverSelfNotifications() {
            return true;
        }

        @Override
        public void onChange(boolean selfChange) {
            onContentChanged();
        }
    }

    /**
     * Called when the {@link ContentObserver} on the cursor receives a change notification.
     * The default implementation provides the auto-requery logic, but may be overridden by
     * sub classes.
     *
     * @see ContentObserver#onChange(boolean)
     */
    protected void onContentChanged() {
        if (mAutoRequery && mCursor != null && !mCursor.isClosed()) {
            if (false) Log.v("Cursor", "Auto requerying " + mCursor + " due to update");
            mDataValid = mCursor.requery();
        }
    }
}