package com.bjongebl.sogeti.cardviewwithdbandheaders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by BJONGEBL on 10-3-2016.
 */

public class MainViewHolder extends RecyclerView.ViewHolder {
    public TextView mTitle;
    public TextView mDescription;
    public ImageView mImage;
    public MainViewHolder(View view) {
        super(view);
        mTitle = (TextView)view.findViewById(R.id.titleCard);
        mDescription = (TextView)view.findViewById(R.id.descriptionCard);
        mImage = (ImageView)view.findViewById(R.id.thumbnail);
    }
}
