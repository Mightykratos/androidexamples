package com.bjongebl.sogeti.cardviewwithdbandheaders;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AlphabetIndexer;
import android.widget.ImageView;
import android.widget.SectionIndexer;
import android.widget.TextView;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by skyfishjy on 10/31/14.
 */
public class MyListCursorAdapter extends CursorRecyclerViewAdapter<RecyclerView.ViewHolder> implements SectionIndexer{

    private static final int TYPE_HEADER = 1;
    private static final int TYPE_NORMAL = 0;
    private static final int TYPE_COUNT = 2;
    private final AlphabetIndexer indexer;
    private final int[] usedSectionNumbers;
    private Map<Integer, Integer> sectionToOffset;
    private Map<Integer, Integer> sectionToPosition;

    public MyListCursorAdapter(Context context,Cursor cursor){
        super(context, cursor);

        indexer = new AlphabetIndexer(cursor, cursor.getColumnIndexOrThrow(DbHelper.MEDICINE_NAME), "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        sectionToPosition = new TreeMap<Integer, Integer>(); //use a TreeMap because we are going to iterate over its keys in sorted order
        sectionToOffset = new HashMap<Integer, Integer>();

        final int count = super.getItemCount();

        int i;
        //temporarily have a map alphabet section to first index it appears
        //(this map is going to be doing somethine else later)
        for (i = count - 1 ; i >= 0; i--){
            sectionToPosition.put(indexer.getSectionForPosition(i), i);
        }

        i = 0;
        usedSectionNumbers = new int[sectionToPosition.keySet().size()];

        //note that for each section that appears before a position, we must offset our
        //indices by 1, to make room for an alphabetical header in our list
        for (Integer section : sectionToPosition.keySet()){
            sectionToOffset.put(section, i);
            usedSectionNumbers[i] = section;
            i++;
        }

        //use offset to map the alphabet sections to their actual indicies in the list
        for(Integer section: sectionToPosition.keySet()){
            sectionToPosition.put(section, sectionToPosition.get(section) + sectionToOffset.get(section));
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTitle;
        public TextView mDescription;
        public ImageView mImage;
        public ViewHolder(View view) {
            super(view);
            mTitle = (TextView)view.findViewById(R.id.titleCard);
            mDescription = (TextView)view.findViewById(R.id.descriptionCard);
            mImage = (ImageView)view.findViewById(R.id.thumbnail);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        RecyclerView.ViewHolder vh;

        if(viewType == TYPE_NORMAL) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card_view_item, parent, false);
            vh = new MainViewHolder(itemView);
        }
        else
        {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.header_list_item, parent, false);
            vh = new HeaderViewHolder(itemView);
        }

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, Cursor cursor) {
        Card myListItem = Card.fromCursor(cursor);

        if(getItemViewType(cursor.getPosition()) == TYPE_NORMAL) {
            ((MainViewHolder) viewHolder).mTitle.setText(myListItem.getTitle());
            ((MainViewHolder) viewHolder).mDescription.setText(myListItem.getDescription());
            ((MainViewHolder) viewHolder).mImage.setImageResource(myListItem.getImage());
        }
        else
        {
            ((HeaderViewHolder) viewHolder).mTitle.setText(myListItem.getTitle());
        }
    }

    @Override
    public int getPositionForSection(int section) {
        if (! sectionToOffset.containsKey(section)){
            //This is only the case when the FastScroller is scrolling,
            //and so this section doesn't appear in our data set. The implementation
            //of Fastscroller requires that missing sections have the same index as the
            //beginning of the next non-missing section (or the end of the the list if
            //if the rest of the sections are missing).
            //So, in pictorial example, the sections D and E would appear at position 9
            //and G to Z appear in position 11.
            int i = 0;
            int maxLength = usedSectionNumbers.length;

            //linear scan over the sections (constant number of these) that appear in the
            //data set to find the first used section that is greater than the given section, so in the
            //example D and E correspond to F
            while (i < maxLength && section > usedSectionNumbers[i]){
                i++;
            }
            if (i == maxLength) return getItemCount(); //the given section is past all our data

            return indexer.getPositionForSection(usedSectionNumbers[i]) + sectionToOffset.get(usedSectionNumbers[i]);
        }

        return indexer.getPositionForSection(section) + sectionToOffset.get(section);
    }

    @Override
    public int getSectionForPosition(int position) {
        int i = 0;
        int maxLength = usedSectionNumbers.length;

        //linear scan over the used alphabetical sections' positions
        //to find where the given section fits in
        while (i < maxLength && position >= sectionToPosition.get(usedSectionNumbers[i])){
            i++;
        }
        return usedSectionNumbers[i-1];
    }

    @Override
    public Object[] getSections() {
        return indexer.getSections();
    }

    //nothing much to this: headers have positions that the sectionIndexer manages.
    @Override
    public int getItemViewType(int position) {
        if (position == getPositionForSection(getSectionForPosition(position))){
            return TYPE_HEADER;
        } return TYPE_NORMAL;
    }


    @Override
    public int getItemCount() {
        if (getCursor().getCount() != 0){
            //sometimes your data set gets invalidated. In this case getCount()
            //should return 0 and not our adjusted count for the headers.
            //The only way to know if data is invalidated is to check if
            //super.getCount() is 0.
            return getCursor().getCount() + usedSectionNumbers.length;
        }

        return 0;
    }


}
