package com.bjongebl.sogeti.cardviewwithdbandheaders;

import android.database.Cursor;

/**
 * Created by BJONGEBL on 3-3-2016.
 */
public class Card {

    private String title,description;
    private int image;

    public Card() {

    }

    public Card(String title, String description, int image) {
        this.title = title;
        this.description = description;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public static Card fromCursor(Cursor cursor) {
        //TODO return your MyListItem from cursor.
        Card card = new Card(cursor.getString(1),cursor.getString(2),cursor.getInt(3));
        return card;

    }
}
