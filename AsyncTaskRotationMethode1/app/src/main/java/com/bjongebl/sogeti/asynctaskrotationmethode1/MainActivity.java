package com.bjongebl.sogeti.asynctaskrotationmethode1;

import android.app.Activity;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{

    EditText selectionText;
    ListView chooseImageList;
    String[] listOfImages;
    ProgressBar downloadImagesProgress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        selectionText = (EditText) findViewById(R.id.urlSelectionText);
        chooseImageList = (ListView) findViewById(R.id.chooseImageList);
        listOfImages = getResources().getStringArray(R.array.imageUrls);
        downloadImagesProgress = (ProgressBar) findViewById(R.id.downloadProgress);
        chooseImageList.setOnItemClickListener(this);
    }

    public void downloadImage(View view) {
        if(selectionText.getText().toString() != null && selectionText.getText().toString().length() > 0){
            MyTask myTask = new MyTask();
            myTask.execute(selectionText.getText().toString());
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        selectionText.setText(listOfImages[position]);
    }

    class MyTask extends AsyncTask<String, Integer, Boolean>{

        private int contentLength = -1;
        private int counter = 0;
        private int calculatedProgress =0;
        private Activity activity;
        @Override
        protected void onPreExecute() {
            downloadImagesProgress.setVisibility(View.VISIBLE);
            if(MainActivity.this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
                MainActivity.this.setRequestedOrientation(Configuration.ORIENTATION_PORTRAIT);
            }else{
                MainActivity.this.setRequestedOrientation(Configuration.ORIENTATION_LANDSCAPE);
            }
        }

        @Override
        protected Boolean doInBackground(String... params) {
            boolean successful = false;
            URL downloadURL = null;
            HttpURLConnection httpURLConnection = null;
            InputStream inputStream = null;
            FileOutputStream fileOutputStream = null;
            File file = null;
            try{
                downloadURL = new URL(params[0]);
                httpURLConnection = (HttpURLConnection) downloadURL.openConnection();
                contentLength = httpURLConnection.getContentLength();
                inputStream = httpURLConnection.getInputStream();

                file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath() + "/" + Uri.parse(params[0]).getLastPathSegment());
                fileOutputStream = new FileOutputStream(file);
                Messenger.logMessage(file.getAbsolutePath());
                int read = -1;
                byte[] buffer = new byte[1024];
                while((read = inputStream.read(buffer)) != -1){
                    fileOutputStream.write(buffer, 0, read);
                    counter = counter + read;
                    publishProgress(counter);
                }
                successful = true;
            } catch (IOException e) {
                Messenger.logMessage(e.toString());
            }finally {
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
                try {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                    if (fileOutputStream != null) {
                        fileOutputStream.close();
                    }
                } catch (IOException e) {
                    Messenger.logMessage(e.toString());
                }
            }
            return successful;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            calculatedProgress = (int)(((double)values[0]/contentLength)*100);
            downloadImagesProgress.setProgress(calculatedProgress);
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            downloadImagesProgress.setVisibility(View.GONE);
        }
    }
}
