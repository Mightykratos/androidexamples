package com.bjongebl.sogeti.asynctaskrotationmethode1;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by BJONGEBL on 15-3-2016.
 */
public class Messenger {
    public static void logMessage(String message){
     Log.d("test", message);
    }
    public static void toastMessage(Context context,String message){
        Toast.makeText(context,message,Toast.LENGTH_LONG).show();
    }
}
