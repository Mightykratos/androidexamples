package com.bjongebl.sogeti.dialogexample;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements MyDialog.Communicator {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void showDialog(View view) {
        FragmentManager manager = getSupportFragmentManager();
        MyDialog myDialog = new MyDialog();
        myDialog.show(manager,"MyDialog");
    }

    @Override
    public void onDialogMessage(String text) {
        Toast.makeText(this,text,Toast.LENGTH_LONG).show();
    }
}
