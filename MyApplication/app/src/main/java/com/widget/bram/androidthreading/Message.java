package com.widget.bram.androidthreading;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by Bram on 14-3-2016.
 */
public class Message {
    public static void logMessage(String message) {
        Log.d("Test", message);
    }

    public static void toastMessage(Context context, String message){
        Toast.makeText(context,message,Toast.LENGTH_LONG).show();
    }
}
