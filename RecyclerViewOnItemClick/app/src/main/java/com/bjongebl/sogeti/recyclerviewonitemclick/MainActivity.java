package com.bjongebl.sogeti.recyclerviewonitemclick;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements BramAdapter.ClickListener {

    private RecyclerView recyclerView;
    private BramAdapter bramAdapter;
    private BramAdapter.ClickListener clickListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        bramAdapter = new BramAdapter(this,getData());
        bramAdapter.setClickListner(this);
        recyclerView.setAdapter(bramAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    public static List<Information> getData(){
        List<Information> data = new ArrayList<>();
        int icons = R.mipmap.ic_launcher;
        String[] title = {"Bram","Gijs","Niels","Bart","Jan","Arnd"};
        for (int i = 0; i < 100; i++) {
            Information current = new Information();
            current.imageId = icons;
            current.title = title[i%title.length];
            data.add(current);
        }
        return data;
    }

    @Override
    public void itemClicked(View view, int position) {
        startActivity(new Intent(getApplicationContext(),Scherm2.class));
    }
}
