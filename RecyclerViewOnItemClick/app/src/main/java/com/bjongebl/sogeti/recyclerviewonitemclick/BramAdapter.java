package com.bjongebl.sogeti.recyclerviewonitemclick;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

/**
 * Created by BJONGEBL on 22-3-2016.
 */
public class BramAdapter extends RecyclerView.Adapter<BramAdapter.MyViewHolder> {

    private Context context;
    private LayoutInflater inflate;
    List<Information> data = Collections.emptyList();
    private ClickListener clickListener;

    public BramAdapter(Context context, List<Information> data) {
        inflate = LayoutInflater.from(context);
        this.data = data;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflate.inflate(R.layout.custom_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.textView.setText(data.get(position).title);
        holder.imageView.setImageResource(data.get(position).imageId);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView textView;
        ImageView imageView;
        public MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            textView = (TextView) itemView.findViewById(R.id.listText);
            imageView = (ImageView) itemView.findViewById(R.id.listIcon);
        }

        @Override
        public void onClick(View v) {
            if(clickListener != null){
                clickListener.itemClicked(v,getAdapterPosition());
            }
        }
    }

    public void setClickListner(ClickListener clickListner){
        this.clickListener = clickListner;
    }
    public interface ClickListener{
        public void itemClicked(View view, int position);
    }
}
