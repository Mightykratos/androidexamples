package com.sogeti.bram.butterknifeexample.Controller.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.sogeti.bram.butterknifeexample.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.title) TextView title;
    @BindView(R.id.subtitle) TextView subText;
    @BindView(R.id.footer) TextView footer;
    @BindView(R.id.button) Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.button) void onClick(){
        Toast.makeText(this, "You pressed me", Toast.LENGTH_LONG).show();
    }
}
