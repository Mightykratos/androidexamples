package com.bjongebl.sogeti.fragmenttransactions;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentB extends Fragment {


    public FragmentB() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_b, container, false);
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d("test", "Fragment B onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("test", "Fragment B onCreate");
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("test", "Fragment B onActivityCreated");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("test", "Fragment B onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("test", "Fragment B onStop");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("test", "Fragment B onDestroyView");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("test", "Fragment B onDestroy");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d("test", "Fragment B onDetach");
    }


}
