package com.bjongebl.sogeti.fragmenttransactions;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    FragmentManager fragmentManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragmentManager = getSupportFragmentManager();
    }

    public void addA(View v){
        FragmentA fragmentA = new FragmentA();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.group,fragmentA,"A");
        transaction.commit();
    }
    public void addB(View v){
        FragmentB fragmentB = new FragmentB();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.group,fragmentB,"B");
        transaction.commit();
    }
    public void removeA(View v){
        FragmentA fragmentA = (FragmentA) fragmentManager.findFragmentByTag("A");
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if(fragmentA!=null) {
            transaction.remove(fragmentA);
            transaction.commit();
        }else{
            Toast.makeText(this,"The Fragment A was not added befor",Toast.LENGTH_SHORT).show();
        }
    }
    public void removeB(View v){
        FragmentB fragmentB = (FragmentB) fragmentManager.findFragmentByTag("B");
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if(fragmentB!=null) {
            transaction.remove(fragmentB);
            transaction.commit();
        }else{
            Toast.makeText(this,"The Fragment B was not added befor",Toast.LENGTH_SHORT).show();
        }
    }

    public void replaceAWithB(View view) {
        FragmentB fragmentB = new FragmentB();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.group,fragmentB,"B");
        transaction.commit();
    }

    public void replaceBwithA(View view) {
        FragmentA fragmentA = new FragmentA();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.group,fragmentA,"A");
        transaction.commit();
    }

    public void attachA(View view) {
        FragmentA fragmentA = (FragmentA) fragmentManager.findFragmentByTag("A");
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if(fragmentA!=null) {
            transaction.attach(fragmentA);
            transaction.commit();
        }
    }

    public void detachA(View view) {
        FragmentA fragmentA = (FragmentA) fragmentManager.findFragmentByTag("A");
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if(fragmentA!=null) {
            transaction.detach(fragmentA);
            transaction.commit();
        }
    }
}
